//-----------------------------------------------------------------
// Name:	Coulston
// File:	lab5.c
// Date:	Fall 2014
// Purp:	Demo the decoding of an IR packet
//-----------------------------------------------------------------
#include <msp430g2553.h>
#include "start5.h"

extern void initMSP();
extern void Delay40ms();
extern void Delay160ms();
extern void initLCD();
extern void clearScreen();
extern void drawBox(unsigned int col, unsigned int row, unsigned int color);

#define		TRUE			1
#define		FALSE			0

int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32	irPacket;

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {

	unsigned int	x, y, color, button_press;

	initMSP430();
	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	Delay160ms();

	x=0;		y=0;		color=45555;
	drawBox(x, y, color);

	while(1) {
		if (newIrPacket) {
			newIrPacket = FALSE;

			if (irPacket == UPBUTT && y >= 10){
				y = y - 10;
				drawBox(x, y, color);
				Delay40ms();
			}
			else if (irPacket == DOWNBUTT && y <= 300){
				y = y + 10;
				drawBox(x, y, color);
				Delay40ms();
			}
			else if (irPacket == LEFTBUTT && x >= 10){
				x = x - 10;
				drawBox(x, y, color);
				Delay40ms();
			}
			else if (irPacket == RIGHTBUTT && x <= 220){
				x = x + 10;
				drawBox(x, y, color);
				Delay40ms();
			}
			else if (irPacket == HOMEBUTT){
				if (color == 45555){
					color = 0000;
				}
				else if (color == 0000){
					color = 45555;
				}
				drawBox(x, y, color);
				Delay40ms();
			}
			irPacket = 0;
		} // end if new IR packet arrived
	} // end infinite loop
} // end main

// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 		// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2SEL &= ~BIT6;						// Set up P2.6 as GPIO not XIN
	P2SEL2 &= ~BIT6;					// Once again, this take three lines
	P2DIR &= ~BIT6; 					// to properly do

	P2IFG &= ~BIT6;						// Clear any interrupt flag on P2.3
	P2IE  |= BIT6;						// Enable P2.3 interrupt

	HIGH_2_LOW;							// check the header out.  P2IES changed.
	P1DIR |= BIT0|BIT6;					// Set LEDs as outputs
	P1OUT &= ~(BIT0|BIT6);				// And turn the LEDs off

	TA0CCR0 = 0xFFFF;					// create a 16ms roll-over period
	TACTL &= ~TAIFG;					// clear flag before enabling interrupts = good practice
	TA0CTL |= TASSEL1|ID_3|MC1;			// Use 1:8 prescalar off SMCLK and enable interrupts

	_enable_interrupt();
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR			// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {

	int8	pin;
	int16	pulseDuration;			// The timer is 16-bits

	if (IR_PIN)		pin=1;	else pin=0;

	switch (pin) {					// read the current pin level
		case 0:						// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
			pulseDuration = TA0R;	//**Note** If you don't specify TA1 or TA0 then TAR defaults to TA0R
			if (pulseDuration > minStartPulse && pulseDuration < maxStartPulse){
				packetIndex = 0;
			}
			if ((pulseDuration > minLogic0Pulse) && (pulseDuration < maxLogic0Pulse)) {
				irPacket = (irPacket << 1) | 0;
			}
			if ((pulseDuration > minLogic1Pulse) && (pulseDuration < maxLogic1Pulse)) {
				irPacket = (irPacket << 1) | 1;
			}
			packetData[packetIndex++] = pulseDuration;
			TACTL = 0;
			LOW_2_HIGH; 				// Set up pin interrupt on positive edge
			break;

		case 1:							// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
			TA0R = 0x0000;						// time measurements are based at time 0
			TACTL = TASSEL1|ID_3|MC1|TAIE;
			HIGH_2_LOW;							// Set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT6;			// Clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// -----------------------------------------------------------------------
//			0 half-bit	1 half-bit		TIMER A COUNTS		TIMER A COUNTS
//	Logic 0	xxx
//	Logic 1
//	Start
//	End
//
// -----------------------------------------------------------------------
#pragma vector = TIMER0_A1_VECTOR			// This is from the MSP430G2553.h file
__interrupt void timerOverflow (void) {

	TACTL = 0;
	TACTL &= ~TAIE;
	packetIndex = 0;
	newIrPacket = TRUE;
	TA0CTL &= ~TAIFG;
}
