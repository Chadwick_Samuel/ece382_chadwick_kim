#Lab 4 - C - "Etch-a-Sketch and Pong"

##Objectives
Use C to create an etch-a-sketch-type program that utilizes some subroutines from Lab 3.  Transition from programming in assembly language to C programming.  

##Purpose
This lab outlines how lower-level languages such as assembly can interface with higher-level languages such as C to create a user experience on specific hardware.  By including previously written assembly, functions which operate the MSP430 LCD do not need to be re-written in C to be interpreted by the Code Composer Studio compiler; instead these hardware interfacing functions and subroutines can be expounded to allow for a programmer to abstract away the hardware requirements when developing an application.  

## Prelab

### Data types

| Size | Signed/Unsigned | Type | Min value | Max value |
| :---: | :---: | :---: | :---: | :---: |
| 8-bit | unsigned | unsigned char | 0 | 255 |
| 8-bit | signed | char | -128 | 127 |
| 16-bit | unsigned | unsigned short | 0 | 65,535 |
| 16-bit | signed | short | -32,768| 32,767 |
| 32-bit | unsigned | unsigned long | 0 | 4,294,967,295 |
| 32-bit | signed | long | -2,147,483,648 | 2,147,483,647 |
| 64-bit | unsigned | unsigned long long| 0 | 18,446,744,073,709,551,615 |
| 64-bit | signed | long long | -9,223,372,036,854,775,808 | 9,223,372,036,854,775,807 |  |

| Type | Meaning | C typedef declaration |
| :---: | :---: | :---: |
| int8 | unsigned 8-bit value | typedef unsigned char int8;|
| sint8 | signed 8-bit value | typedef char sint8;|
| int16 | unsigned 16-bit value | typedef unsigned short int16;|
| sint16 | signed 16-bit value | typedef short sint16;|
| int32 | unsigned 32-bit value | typedef unsigned long int32;|
| sint32 | signed 32-bit value | typedef long sint32;|
| int64 | unsigned 64-bit value | typedef unsigned long long int64;|
| sint64 | signed 64-bit value | typedef long long sint64;|  |

### Calling/Return Convention

| Iteration | a | b | c | d | e |
| :---: | :---: | :---: | :---: | :---:| :---: |
| 1st | 1 | 2 | 3 | 4 | 2 | 
| 2nd | 10 | 9 | 8 | 7 | 8 | 
| 3rd | 16 | 15 | 14 | 13 | 14 | 
| 4th | 22 | 21 | 20 | 19 | 20 | 
| 5th | 28 | 27 | 26 | 25 | 26 |  |

| Parameter | Value Sought |
| :---: | :---: |
| Starting address of `func` | 0xC058 |
| Ending address of `func` | 0xC08C |
| Register holding w | 12 |
| Register holding x | 13 |
| Register holding y | 14 |
| Register holding z | 15 |
| Register holding return value | 12 | |

### Cross language build constructs

Answer the following questions:

What is the role of the `extern` directive in a .c file?

It explicitly declares a variable, without a definition, so that it can used as an external variable. You can then use it inside a function, where the variable is then used as a local variable for that particular function. It is defined outside, so that other functions can use this variable.  

What is the role of the `.global` directive in an .asm file (used in lines 60-64)?  Hint: reference section 2.6.2 in the MSP 430 Assembly Language Tools v4.3 User's Guide.

The role of the .global directive makes a reference, in which other object modules asre able to see it. It can be defined in one file and referenced in another file. It makes the linker look for the symbol in other files. If it can't find it, then an unresolved reference error is given.

### Required Functionality

Modify your assembly drawBox function to take in 3 values: an x coordinate, a y coordinate, and a color.  

Click [here](https://www.youtube.com/watch?v=6bTWdnOKcb0) for our Etch-a-Sketch demo!

#### Modified Assembly Code

	;-------------------------------------------------------------------------------
	;	Name: drawBox
	;	Inputs: xStart in r12, yStart in r13, box color in r14
	;	Outputs: none
	;	Purpose: draw a 10x10 box
	;	Registers:
	;-------------------------------------------------------------------------------
	drawBox:
		push	r6
		push	r7
		push	r8
		push	r12
		push	r13
		push	r14
		push	r15
		mov		r14, r6
		mov		r12, r14
		mov		r13, r15
		add		#9, r14
		add		#9, r15
		call	#setArea
		mov		r6, r12	;0x0000 for black or 0xFFFFF for white; other colors in between
		call	#splitColor	;returns r12 with MSB of color, r13 the LSB
	
		mov		#20, r7
		mov		#18, r8

Some hints have been included in the lab4.c file to get you started.  Create an etch-a-sketch program using the directional buttons of the LCD boosterpack to control the position of the paint brush. The paint brush will draw 10x10 blocks of pixels. The user will change the position of the paint brush by pressing the directional buttons. Each button press will move the cursor 10 pixels in the direction pressed (see table below). Pressing the auxiliary button (S1) will toggle the mode of the paint brush between filling squares and clearing squares.

| Button | Function |
| --- | --- |
| S4/Up |	Move the cursor up 1 block |
| S3/Down	| Move the cursor down 1 block |
| S2/Left	| Move the cursor left 1 block |
| S5/Right | Move the cursor right 1 block |
| S1/Aux | Toggle the color of the paint brush |

####Modified C Code

	void main() {
	
		unsigned int	x, y, color, button_press;
	
		// === Initialize system ================================================
		IFG1=0; /* clear interrupt flag1 */
		WDTCTL=WDTPW+WDTHOLD; /* stop WD */
		button_press = FALSE;
	
		initMSP();
		Delay160ms();
		initLCD();
		Delay160ms();
		clearScreen();
		Delay160ms();
		
		x=0;		y=0;		color=45555;
		drawBox(x, y, color);
	
		while(1) {
			if (UP_BUTTON == 0 && y >= 10){
				y = y - 10;
				drawBox(x, y, color);
				Delay160ms();
			}
			else if (DOWN_BUTTON == 0 && y <= 300){
				y = y + 10;
				drawBox(x, y, color);
				Delay160ms();
			}
			else if (LEFT_BUTTON == 0 && x >= 10){
				x = x - 10;
				drawBox(x, y, color);
				Delay160ms();
			}
			else if (RIGHT_BUTTON == 0 && x <= 220){
				x = x + 10;
				drawBox(x, y, color);
				Delay160ms();
			}
			else if (RESET_BUTTON == 0){
				if (color == 45555){
					color = 0000;
				}
				else if (color == 0000){
					color = 45555;
				}
				drawBox(x, y, color);
				Delay160ms();
			}
				}
	}

### B Functionality

Create a bouncing block!  This block should move across the screen with no more than 10 pixels per jump.  It should bounce off the walls appropriately, similar to assignment 7.  An adequate delay should be added between each block movement.  Your starting position and starting x and y velocities should be initialized in your header, or should be randomly generated.  

Click [here](https://www.youtube.com/watch?v=MlQX1Olkzuo) for our Bouncing Block demo!

####Modified C Code

	void main() {
	
		unsigned int	x, y, color, button_press, xvel, yvel;
	
		// === Initialize system ================================================
		IFG1=0; /* clear interrupt flag1 */
		WDTCTL=WDTPW+WDTHOLD; /* stop WD */
		button_press = FALSE;
	
		initMSP();
		Delay160ms();
		initLCD();
		Delay160ms();
		clearScreen();
		Delay160ms();
		
		x=50;		y=30;		color=45555;	xvel=5;	yvel=7;
		drawBox(x, y, color);
	
		while(1) {
			if (x+xvel < 0 || x+xvel > 230){
				xvel = -xvel;
			}
			if (y+yvel < 0 || y+yvel > 310){
				yvel = -yvel;
			}
			drawBox(x,y,0000);
			x+=xvel;
			y+=yvel;
			drawBox(x,y,color);
			Delay160ms();
				}
	}

### A Functionality

Create Pong on your display! Create a single paddle that will move up and down (or side to side) on one side of the display, controlled by the up and down (or left and right) buttons.  The block will bounce off the paddle like it bounces off the wall.  When the block misses hitting the paddle, the game will end.  
  
Click [here](https://www.youtube.com/watch?v=3o11cNDKo0c) for a Pong demo!

####Modified C Code

	void main() {
	
		unsigned int	x, y, color, button_press, xvel, yvel, xpad, ypad;
	
		// === Initialize system ================================================
		IFG1=0; /* clear interrupt flag1 */
		WDTCTL=WDTPW+WDTHOLD; /* stop WD */
		button_press = FALSE;
	
		initMSP();
		Delay160ms();
		initLCD();
		Delay160ms();
		clearScreen();
		Delay160ms();
		
		x=50;		y=30;		color=45555;	xvel=5;		yvel=7;		xpad = 200;		ypad = 270;
		drawBox(x, y, color);
		drawPad(xpad, ypad, color);
	
	
		while(1) {
			if (x+xvel < 0 || x+xvel > xpad-10){
				xvel = -xvel;
			}
			if (x == xpad-10){
				if (y+yvel < ypad || y+yvel > ypad+50){
					break;
				}
			}
			if (y+yvel < 0 || y+yvel > 310){
				yvel = -yvel;
			}
			drawBox(x,y,0000);
			x+=xvel;
			y+=yvel;
			drawBox(x,y,color);
			if (UP_BUTTON == 0 && ypad >= 10){
				drawPad(xpad, ypad, 0000);
				ypad = ypad - 10;
				drawPad(xpad, ypad, color);
			}
			else if (DOWN_BUTTON == 0 && ypad <= 260){
				drawPad(xpad, ypad, 0000);
				ypad = ypad + 10;
				drawPad(xpad, ypad, color);
			}
			Delay40ms();
		}
	}

### Bonus Functionality

Each bonus functionality can be achieved in conjunction with either A or B functionality.  Each is worth 5 points.

####Changing Pong Ball Color
The color of the pong ball changes upon impact with the screen edge.  
[See this in action!](https://www.youtube.com/watch?v=E36CbiXPoxk)

##Debugging

####Git Bash and SourceTree  
Initially our combined repository was created in Git bash; however, we quickly learned that by simultaneously programming in the same file, our file would be corrupted with the reference data from specific Git pushes and pulls.  Our solution required us to work on files separately and communicate with each other on who was working on what and when.  

####Required Functionality  
The first issue that we ran into when doing the required functionality was that nothing would show up on the screen. However, C2C Justin Bloomis explained that in order for my board to initialize, we needed to add a delay between the initialization of the screen and the clear screen. When we added the delay, the board was able to display our programmed. We ran into another issue when creating polling functions for the buttons. we assigned the wrong pins and bits for the buttons which caused the controls to move in different directions than what we set them as. So the left button would move the box up, while the right button moved the box down. We were able to reference the lab 3 prelab and found that we assigned the pins and bits incorrectly for polling. The third issue we ran into when programming was that the box would leave the display, and in order to fix that, we added a check to see if the box went over the edge or not; if it did, the drawing of the box would stop. Once implemented, we had the required functionality working.  

####B Functionality  
B functionality was a bit more challenging because we had to assign all 4 walls for the display. We went through guessing and checking to see when the box would hit the sides and using hard-coded screen edge values. We then made a condition such that when the box hits the edge, it would flip the velocity to the other direction (negative to positive, positive to negative).  

####A Functionality  
A functionality required even more problem solving; we had to create an interactive paddle. C2C Kim's thought process on the paddle was that we would just use the drawBox function; but instead of drawing a 10x10 box, we would draw a much thinner box (a 3x50 box). The next issue we ran into was that we were using the x and y coordinates of the box in the paddle, resulting in them overlapping each other. We had to create extra variables for the paddle so that the box and the paddle did not overlap each other. Once these variables were created, we worked towards implementing conditions such that the game would stop if the paddle missed the ball. We tried to create conditions for the y coordinates alone, but that caused the ball to always hit the paddle. Instead, we thought of another way to check these cases. The first cases was that the box must be at the x-coordinate of the paddle. Once the ball is at the x-coordinate of the paddle, we know that the box is either touching the paddle or not touching the paddle. We then created a nested condition such that it would first check the x coordinate and then check the y coordinate to see if our ball was within the paddle. Once the conditions were set, the command would break, which would allow our program to jump out of the while loop, causing our program to end. Another issue we ran into was that the program would slow down dramatically when moving the paddle. In order to correct this, Capt Warner recommend that we remove any unnecessary delays in the while loop and remove delays for each button press. All that remained was one delay at the end of the while loop, which then allowed our program to only have a single delay for the whole loop. This caused our program to run much faster and for the paddle movement to not affect the movement of the ball.  

##Observations and Conclusions
This lab successfully demonstrated that  the culmination of hardware, lower-level language, and higher-level programming combine to create user-friendly programs.  A thorough understanding of each "layer" of implementation is crucial to abstracting the final design user interface (buttons) from the C programming, Assembly initialization of the LCD display, and the hardware facilitation of information processing.  By comprehending the interactions between each "layer" of abstraction, functions and applications can be developed, tested, and implemented at a pace much quicker and with a functionality much greater than that of one which is only dependent upon one (hardware, assembly, or C).  

##Documentation
ECE 382 Embedded System I Resource Handbook "Blue Book" - Used for instruction set descriptions, SPI configuration, and UCB initialization.  
ece.ninja/382 - All lab 3 resources available on ECENinja.  
ece.ninja/382 - All lab 4 resources available on ECENinja. 
C2C Kim, Hwi Tae (James) - Provided debugging specifics and implimentation and functionality videos for this write-up.  

*Note: Coding documentation contains separate entries regarding the progression of code modification and increases in functionality. Please consult main.c for this particular documentation statement.*