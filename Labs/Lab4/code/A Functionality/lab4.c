/*-------------------------------------------------------------------------------
 Lab 4 - C - Etch A Sketch and Pong
 C2C Hwi Tae Kim, USAF / 25 Oct 2016 / 25 Oct 2016

 This program creates an etch a sketch and pong into our MSP430.


 Documentation: C2C Justin Bloomis explained how I can just add codes to the lab4.c
-------------------------------------------------------------------------------
*/
#include <msp430g2553.h>

extern void initMSP();
extern void Delay160ms();
extern void Delay40ms();
extern void initLCD();
extern void clearScreen();
extern void drawBox(unsigned int col, unsigned int row, unsigned int color);
extern void drawPad(unsigned int col, unsigned int row, unsigned int color);

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		RESET_BUTTON	(P1IN & BIT3)

void main() {

	unsigned int	x, y, color,padcolor, button_press, xvel, yvel, xpad, ypad;

	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL=WDTPW+WDTHOLD; /* stop WD */
	button_press = FALSE;

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	Delay160ms();
	
	x=50;		y=30;		color=45555;	padcolor = 45555;	xvel=5;		yvel=7;		xpad = 200;		ypad = 270;
	drawBox(x, y, color);
	drawPad(xpad, ypad, padcolor);


	while(1) {
		if (x+xvel < 0 || x+xvel > xpad-10){
			xvel = -xvel;
			color = color - 30;
		}
		if (x == xpad-10){
			if (y+yvel < ypad || y+yvel > ypad+50){
				break;
			}
		}
		if (y+yvel < 0 || y+yvel > 310){
			yvel = -yvel;
			color = color - 50;
		}
		drawBox(x,y,0000);
		x+=xvel;
		y+=yvel;
		drawBox(x,y,color);
		if (UP_BUTTON == 0 && ypad >= 10){
			drawPad(xpad, ypad, 0000);
			ypad = ypad - 10;
			drawPad(xpad, ypad, padcolor);
		}
		else if (DOWN_BUTTON == 0 && ypad <= 260){
			drawPad(xpad, ypad, 0000);
			ypad = ypad + 10;
			drawPad(xpad, ypad, padcolor);
		}
		Delay40ms();
	}
}
