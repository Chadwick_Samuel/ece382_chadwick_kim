/*-------------------------------------------------------------------------------
 Lab 4 - C - Etch A Sketch and Pong
 C2C Hwi Tae Kim, USAF / 25 Oct 2016 / 25 Oct 2016

 This program creates an etch a sketch and pong into our MSP430.


 Documentation: C2C Justin Bloomis explained how I can just add codes to the lab4.c
-------------------------------------------------------------------------------
*/
#include <msp430g2553.h>

extern void initMSP();
extern void Delay160ms();
extern void initLCD();
extern void clearScreen();
extern void drawBox(unsigned int col, unsigned int row, unsigned int color);

#define		TRUE			1
#define		FALSE			0
#define		UP_BUTTON		(P2IN & BIT2)
#define		DOWN_BUTTON		(P2IN & BIT1)
#define		LEFT_BUTTON		(P2IN & BIT0)
#define		RIGHT_BUTTON	(P2IN & BIT3)
#define		RESET_BUTTON	(P1IN & BIT3)

void main() {

	unsigned int	x, y, color, button_press;

	// === Initialize system ================================================
	IFG1=0; /* clear interrupt flag1 */
	WDTCTL=WDTPW+WDTHOLD; /* stop WD */
	button_press = FALSE;

	initMSP();
	Delay160ms();
	initLCD();
	Delay160ms();
	clearScreen();
	Delay160ms();
	
	x=0;		y=0;		color=45555;
	drawBox(x, y, color);

	while(1) {
		if (UP_BUTTON == 0 && y >= 10){
			y = y - 10;
			drawBox(x, y, color);
			Delay160ms();
		}
		else if (DOWN_BUTTON == 0 && y <= 300){
			y = y + 10;
			drawBox(x, y, color);
			Delay160ms();
		}
		else if (LEFT_BUTTON == 0 && x >= 10){
			x = x - 10;
			drawBox(x, y, color);
			Delay160ms();
		}
		else if (RIGHT_BUTTON == 0 && x <= 220){
			x = x + 10;
			drawBox(x, y, color);
			Delay160ms();
		}
		else if (RESET_BUTTON == 0){
			if (color == 45555){
				color = 0000;
			}
			else if (color == 0000){
				color = 45555;
			}
			drawBox(x, y, color);
			Delay160ms();
		}
			}
}
