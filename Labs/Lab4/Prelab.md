#Lab 4 - C - "Etch-a-Sketch and Pong"

## Prelab

### Data types

| Size | Signed/Unsigned | Type | Min value | Max value |
| :---: | :---: | :---: | :---: | :---: |
| 8-bit | unsigned | unsigned char | 0 | 255 |
| 8-bit | signed | char | -128 | 127 |
| 16-bit | unsigned | unsigned short | 0 | 65,535 |
| 16-bit | signed | short | -32,768| 32,767 |
| 32-bit | unsigned | unsigned long | 0 | 4,294,967,295 |
| 32-bit | signed | long | -2,147,483,648 | 2,147,483,647 |
| 64-bit | unsigned | unsigned long long| 0 | 18,446,744,073,709,551,615 |
| 64-bit | signed | long long | -9,223,372,036,854,775,808 | 9,223,372,036,854,775,807 |  |

| Type | Meaning | C typedef declaration |
| :---: | :---: | :---: |
| int8 | unsigned 8-bit value | typedef unsigned char int8;|
| sint8 | signed 8-bit value | typedef char sint8;|
| int16 | unsigned 16-bit value | typedef unsigned short int16;|
| sint16 | signed 16-bit value | typedef short sint16;|
| int32 | unsigned 32-bit value | typedef unsigned long int32;|
| sint32 | signed 32-bit value | typedef long sint32;|
| int64 | unsigned 64-bit value | typedef unsigned long long int64;|
| sint64 | signed 64-bit value | typedef long long sint64;|  |

### Calling/Return Convention

| Iteration | a | b | c | d | e |
| :---: | :---: | :---: | :---: | :---:| :---: |
| 1st | 1 | 2 | 3 | 4 | 2 | 
| 2nd | 10 | 9 | 8 | 7 | 8 | 
| 3rd | 16 | 15 | 14 | 13 | 14 | 
| 4th | 22 | 21 | 20 | 19 | 20 | 
| 5th | 28 | 27 | 26 | 25 | 26 |  |

| Parameter | Value Sought |
| :---: | :---: |
| Starting address of `func` | 0xC058 |
| Ending address of `func` | 0xC08C |
| Register holding w | 12 |
| Register holding x | 13 |
| Register holding y | 14 |
| Register holding z | 15 |
| Register holding return value | 12 | |

### Cross language build constructs

Answer the following questions:

What is the role of the `extern` directive in a .c file?

It explicitly declares a variable, without a definition, so that it can used as an external variable. You can then use it inside a function, where the variable is then used as a local variable for that particular function. It is defined outside, so that other functions can use this variable.  

What is the role of the `.global` directive in an .asm file (used in lines 60-64)?  Hint: reference section 2.6.2 in the MSP 430 Assembly Language Tools v4.3 User's Guide.

The role of the .global directive makes a reference, in which other object modules asre able to see it. It can be defined in one file and referenced in another file. It makes the linker look for the symbol in other files. If it can't find it, then an unresolved reference error is given.

Documentation:
None